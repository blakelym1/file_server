import tempfile
from pathlib import Path
from typing import Callable, Generator, List, Tuple

import pytest
from fastapi import UploadFile
from fastapi.testclient import TestClient

from framework.config import AppSettings, build_app_settings
from framework.main import app
from framework.utils import temp_set_env_vals

TESTING_CONFIG_FILENAME = "config.testing.toml"


@pytest.fixture(scope="function")
def generate_upload_files() -> Callable[[int], Generator[UploadFile, None, None]]:
    def _generate_upload_files(file_size: int) -> Generator[UploadFile, None, None]:
        id = 0
        while True:
            temp_file = tempfile.SpooledTemporaryFile(
                max_size=UploadFile.spool_max_size
            )
            temp_file.write(b"a" * file_size)
            temp_file.flush()
            temp_file.seek(0)
            file = UploadFile(filename=f"file{id}", file=temp_file)  # type: ignore
            yield file
            id += 1

    return _generate_upload_files


@pytest.fixture(scope="function")
def app_settings(tmp_path: Path) -> Generator[AppSettings, None, None]:

    with temp_set_env_vals(
        {
            "root_dir": str(tmp_path),
            "config_filepath": f"{Path(__file__).parent / TESTING_CONFIG_FILENAME}",
        }
    ):

        yield build_app_settings()

        # clear cache to update new root dir based on the specific test
        build_app_settings.cache_clear()


@pytest.fixture(scope="function")
def create_experiment_dirs(app_settings: AppSettings) -> Tuple[Path, List[str]]:
    experiment_ids: List[str] = list(str(x) for x in range(10))
    experiment_ids.append("a string id")
    data_dir = Path(app_settings.data_dir)

    for exp_id in experiment_ids:
        exp_path = data_dir / str(exp_id)
        exp_path.mkdir(parents=True)

    return data_dir, experiment_ids


@pytest.fixture(scope="function")
def test_client() -> Generator[TestClient, None, None]:
    client = TestClient(app)
    yield client
