from pathlib import Path
from unittest.mock import Mock, patch

import pytest

from framework.config import AppSettings, EnvSettings, build_app_settings
from framework.utils import temp_set_env_vals


def test_env_settings_defaults():

    expected_root_dir = Path(__file__).parents[1]
    expected_config_filepath = (
        Path(__file__).parents[1] / "src" / "framework" / "config.toml"
    )

    with temp_set_env_vals(
        {
            "root_dir": None,
            "config_filepath": None,
        }
    ):
        env_settings = EnvSettings()

    assert Path(env_settings.root_dir) == expected_root_dir
    assert Path(env_settings.config_filepath) == expected_config_filepath


def test_env_settings_from_envvar(tmp_path: Path):
    expected_root_dir = tmp_path
    expected_config_filepath = tmp_path / "test"
    with temp_set_env_vals(
        {
            "root_dir": str(expected_root_dir),
            "config_filepath": str(expected_config_filepath),
        }
    ):
        env_settings = EnvSettings()
    assert Path(env_settings.root_dir) == expected_root_dir
    assert Path(env_settings.config_filepath) == expected_config_filepath


@pytest.mark.parametrize(
    "root_dir_str, data_dir_str", [("/tmp", "test/data"), ("/tmp", "/opt/test/data")]
)
def test_app_settings_load(root_dir_str: str, data_dir_str: str):
    app_settings_dict = {"data_dir": data_dir_str}

    app_settings = AppSettings.load(
        root_dir_str=root_dir_str, app_settings_dict=app_settings_dict
    )
    root_dir = Path(root_dir_str)
    data_dir = Path(data_dir_str)

    if data_dir.is_absolute() is False:
        assert app_settings.data_dir == root_dir / data_dir
    else:
        assert app_settings.data_dir == data_dir

    assert app_settings.root_dir == root_dir


def test_build_app_settings():
    root_dir = "/tmp"
    data_dir = "test/data"
    mock_env_settings = Mock(root_dir=root_dir, config_filepath="/tmp/config.toml")
    mock_env_settings.load_config = Mock(
        spec=EnvSettings.load_config, return_value={"data_dir": data_dir}
    )

    # skip the loading from env and from file
    with patch(
        "framework.config.EnvSettings.load", Mock(return_value=mock_env_settings)
    ):
        app_settings = build_app_settings()

    assert app_settings.root_dir == Path(root_dir)
    assert app_settings.data_dir == Path(root_dir) / data_dir
    build_app_settings.cache_clear()
