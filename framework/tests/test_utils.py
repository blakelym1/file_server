import pytest
from typing import Tuple, Dict
from framework.utils import set_or_delete, temp_set_env_vals
import os


class TestSetOrDelete:
    @pytest.fixture
    def key_val(self) -> Tuple[str, ...]:
        return tuple(str(i) for i in range(2))

    def test_set(self, key_val: Tuple[str, str]):
        key, value = key_val
        cache: Dict[str, str] = {}
        set_or_delete(cache, key, value)
        assert cache[key], value

    def test_delete(self, key_val: Tuple[str, str]):
        key, value = key_val
        cache = {key: value}
        set_or_delete(cache, key, None)
        assert key not in cache


class TestTempSetEnvVals:
    def test_empty(self):
        env_key_vals = {}
        env_cache = os.environ.copy()
        with temp_set_env_vals(env_key_vals):
            assert env_cache == os.environ
        assert env_cache == os.environ

    def test_set(self):
        env_key_vals = {str(i): str(i) for i in range(2)}
        env_cache = os.environ.copy()
        with temp_set_env_vals(env_key_vals):
            assert env_key_vals.items() <= os.environ.items()
        assert env_cache == os.environ

    def test_remove(self):
        key = "-1"
        os.environ[key] = "test"
        env_cache = os.environ.copy()
        with temp_set_env_vals({key: None}):
            assert key not in os.environ
        assert env_cache == os.environ

        # cleanup
        del os.environ[key]
