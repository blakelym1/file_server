import io
import zipfile
from io import BytesIO
from pathlib import Path
from typing import List, Tuple

from fastapi.testclient import TestClient

from framework.config import AppSettings


def test_read_main(test_client: TestClient):
    response = test_client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "running"}


def test_list_experiments(
    test_client: TestClient, create_experiment_dirs: Tuple[Path, List[str]]
):
    _, expected_exp_ids = create_experiment_dirs
    response = test_client.get("/experiments")
    assert response.status_code == 200
    assert response.json() == {"experiments": sorted(expected_exp_ids)}


def test_info(test_client: TestClient, app_settings: AppSettings):
    response = test_client.get("/info")
    assert response.status_code == 200
    resp_data = response.json()

    assert Path(resp_data["root_dir"]) == app_settings.root_dir
    assert Path(resp_data["data_dir"]) == app_settings.data_dir


def test_file_post(test_client: TestClient, app_settings: AppSettings):
    buffer = io.BytesIO(b"hello world")
    experiment_id = 42
    filenames = ["foo.pdf", "bar.pdf"]
    response = test_client.post(
        f"/upload_files/{experiment_id}",
        files=[
            ("files", (filenames[0], buffer, "application/pdf")),
            ("files", (filenames[1], buffer, "application/pdf")),
        ],
    )

    # check REST API response
    assert response.status_code == 200
    assert response.json() == {"experiment_id": experiment_id, "filenames": filenames}

    # check files were created
    for filename in filenames:
        filepath = Path(app_settings.data_dir / str(experiment_id) / filename)
        assert filepath.exists()
        assert filepath.read_bytes() == buffer.getvalue()


def test_file_get(test_client: TestClient, app_settings: AppSettings):
    experiment_id = 42
    expected_file_name = "test_file"
    expected_file_content = b"hello_world"
    for filename in [expected_file_name]:
        filepath = Path(app_settings.data_dir / str(experiment_id) / filename)
        filepath.parent.mkdir(exist_ok=True, parents=True)
        filepath.write_bytes(expected_file_content)
    response = test_client.get(
        f"/get_files/{experiment_id}",
    )
    assert response.status_code == 200

    with zipfile.ZipFile(BytesIO(response.content), mode="r") as zf:
        assert [
            expected_file_name,
        ] == zf.namelist()
        assert zf.read(expected_file_name) == expected_file_content
