import pathlib
from anyio import Path
from typing import Callable, Generator, List, Tuple

import pytest
from fastapi import UploadFile

from framework.actions import MB, store_files, get_experiment_ids


@pytest.mark.parametrize("file_size", [8, 2 * MB, 0])
@pytest.mark.parametrize("num_files", [1, 3, 0])
@pytest.mark.anyio
async def test_upload_file_store(
    file_size: int,
    num_files: int,
    tmp_path: pathlib.Path,
    generate_upload_files: Callable[[int], Generator[UploadFile, None, None]],
):
    upload_files = generate_upload_files(file_size)
    files = [next(upload_files) for _ in range(num_files)]
    experiment_id = 0
    await store_files(data_dir=Path(tmp_path), files=files, experiment_id=experiment_id)

    expected_dir = tmp_path / str(experiment_id)
    assert expected_dir.exists()

    for file in files:
        await file.seek(0)
        expected_file = expected_dir / file.filename
        assert expected_file.exists()
        assert expected_file.read_bytes() == await file.read()


@pytest.mark.anyio
async def test_list_experiment_ids(create_experiment_dirs: Tuple[Path, List[str]]):
    tmp_path, expected_exp_ids = create_experiment_dirs

    sorted_actual_exp_ids = sorted(
        [exp_id async for exp_id in get_experiment_ids(Path(tmp_path))]
    )
    sorted_expected_experiment_ids = sorted(expected_exp_ids)

    assert sorted_actual_exp_ids == sorted_expected_experiment_ids
