import io
import os
from contextlib import contextmanager
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path as pPath
from typing import (
    AsyncGenerator,
    AsyncIterable,
    Generator,
    List,
    Literal,
    Mapping,
    MutableMapping,
    Optional,
    Protocol,
    Union,
)
from zipfile import ZIP_DEFLATED, ZipFile, ZipInfo
from zlib import DEF_BUF_SIZE

from anyio import Path as aPath
from anyio import create_memory_object_stream, EndOfStream
from anyio.streams.buffered import BufferedByteReceiveStream
from anyio.streams.file import FileReadStream
from anyio.streams.memory import MemoryObjectSendStream
from typing_extensions import runtime_checkable


@runtime_checkable
class BytesReadable(Protocol):
    def read(self, size: int) -> bytes:
        ...


@runtime_checkable
class BytesRecievable(Protocol):
    async def receive(self, max_bytes: int) -> bytes:
        ...


class NotSeekableStream(io.RawIOBase):
    def __init__(self) -> None:
        self._buffer = b""

    def writable(self) -> Literal[True]:
        return True

    def seekable(self) -> Literal[False]:
        return False

    def write(self, b: bytes) -> int:  # type: ignore
        if self.closed:
            raise ValueError("Stream was closed!")
        before_len = len(self._buffer)
        self._buffer += b
        return len(self._buffer) - before_len

    def read(self, size: Optional[int] = None) -> bytes:
        chunk = self._buffer
        self._buffer = b""
        return chunk


def set_or_delete(d: MutableMapping[str, str], key: str, val: Optional[str]) -> None:
    if val is None and key in d:
        del d[key]
    elif val is not None:
        d[key] = val


@contextmanager
def temp_set_env_vals(
    env_vars: Mapping[str, Optional[str]]
) -> Generator[None, None, None]:
    env_var_cache = {env_key: os.environ.get(env_key) for env_key in env_vars.keys()}

    for key, env_var in env_vars.items():
        set_or_delete(os.environ, key, env_var)

    yield

    for key, val in env_var_cache.items():
        set_or_delete(os.environ, key, val)


@dataclass
class ZipSteamInfo:
    data_stream: Union[BytesReadable, BytesRecievable]
    z_info: ZipInfo

    def __aiter__(self) -> "ZipSteamInfo":
        self.send_stream, self.receive_stream = create_memory_object_stream(
            max_buffer_size=DEF_BUF_SIZE, item_type=bytes
        )
        self.buffered_stream = BufferedByteReceiveStream(self.receive_stream)
        return self

    async def __anext__(self) -> bytes:
        try:
            await self.push_next(self.send_stream, DEF_BUF_SIZE)
        except EndOfStream:
            raise StopAsyncIteration()

        out = await self.buffered_stream.receive()
        if len(out) == 0:
            raise StopAsyncIteration()
        return out

    async def push_next(
        self, memory_stream: MemoryObjectSendStream[bytes], size: int = 0
    ) -> None:
        """push data from a sync or async data stream.

        if sync is used, the data should be in memory.
        """
        if isinstance(self.data_stream, BytesReadable):
            memory_stream.send_nowait(self.data_stream.read(size))
        else:
            await memory_stream.send(await self.data_stream.receive(size))


@dataclass
class ZipFileStream:
    stream_infos: List[ZipSteamInfo]

    @classmethod
    async def from_files(
        cls, file_paths: Union[AsyncIterable[aPath], AsyncIterable[pPath]]
    ) -> "ZipFileStream":
        """create a zip stream from files."""
        zip_stream_infos: List[ZipSteamInfo] = []

        # create a zip stream entry from each of the file's meta data
        async for file_path in file_paths:
            input_file_stream = await FileReadStream.from_path(file_path)
            z_info = ZipInfo.from_file(str(file_path), arcname=file_path.name)
            zip_stream_infos.append(
                ZipSteamInfo(data_stream=input_file_stream, z_info=z_info)
            )

        return cls(zip_stream_infos)

    @classmethod
    async def from_byte_streams(
        cls, data_streams: List[io.BytesIO], file_names: List[str]
    ) -> "ZipFileStream":
        """Create a stream from a list of byte streams and corresponding names.

        Useful for testing or streaming unknown types of data
        """
        zip_stream_infos: List[ZipSteamInfo] = []

        # combine each stream and file_name into a zip stream entry
        for data_stream, file_name in zip(data_streams, file_names):
            z_info = ZipInfo(file_name, date_time=datetime.now().timetuple()[:6])
            zip_stream_infos.append(
                ZipSteamInfo(
                    data_stream=data_stream,
                    z_info=z_info,
                )
            )
        return cls(zip_stream_infos)

    async def __aiter__(self) -> AsyncGenerator[bytes, None]:
        """Return an async stream of bytes in zip block size chunks.

        At the end of the stream, the zip metadata for each entry will be sent.
        """
        zip_byte_stream = NotSeekableStream()
        with ZipFile(
            zip_byte_stream,  # type: ignore
            mode="w",
            compression=ZIP_DEFLATED,
        ) as zf:

            # write each stream to a zip file entry in chunks
            for stream_info in self.stream_infos:
                with zf.open(stream_info.z_info, mode="w") as dest:

                    # returns chunks <= DEF_BUF_SIZE (zip block size)
                    async for chunk in stream_info:
                        dest.write(chunk)

                        # Yield chunk of the zip file stream in bytes.
                        yield zip_byte_stream.read()

        # ZipFile was closed, send archive metadata
        yield zip_byte_stream.read()
