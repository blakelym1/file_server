from typing import List, AsyncGenerator
from fastapi import UploadFile
from anyio import Path, open_file, create_task_group

MB = 1024 * 1024


async def store_single_file(
    upload_file: UploadFile, target_file: Path, chunk_size: int = MB
) -> None:
    async with await open_file(target_file, "wb") as f:
        while chunk := await upload_file.read(chunk_size):
            await f.write(chunk)


async def store_files(
    data_dir: Path, experiment_id: int, files: List[UploadFile]
) -> None:

    exp_dir = Path(data_dir / str(experiment_id))
    await exp_dir.mkdir(exist_ok=True, parents=True)

    async with create_task_group() as tg:
        for upload_file in files:
            tg.start_soon(
                store_single_file, upload_file, exp_dir / upload_file.filename
            )


async def get_experiment_ids(data_dir: Path) -> AsyncGenerator[str, None]:
    async for path in data_dir.iterdir():
        yield path.name


async def get_experiment_files(
    data_dir: Path, experiment_id: int
) -> AsyncGenerator[Path, None]:
    exp_dir = data_dir / str(experiment_id)
    if await exp_dir.exists():
        async for path in exp_dir.iterdir():
            yield path
