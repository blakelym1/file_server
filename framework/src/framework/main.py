from typing import Dict, List, Union

from fastapi import Depends, FastAPI, UploadFile
from fastapi.responses import StreamingResponse

from framework.actions import get_experiment_files, get_experiment_ids, store_files
from framework.config import AppSettings, build_app_settings
from framework.utils import ZipFileStream

app = FastAPI()


@app.get("/")
def root_status() -> Dict[str, str]:
    return {"msg": "running"}


@app.get("/experiments")
async def list_experiments(
    app_settings: AppSettings = Depends(build_app_settings),
) -> Dict[str, List[str]]:

    return {
        "experiments": sorted(
            [exp_id async for exp_id in get_experiment_ids(app_settings.data_dir)]
        )
    }


@app.get("/info")
def get_info(app_settings: AppSettings = Depends(build_app_settings)) -> Dict[str, str]:
    return {
        "root_dir": str(app_settings.root_dir),
        "data_dir": str(app_settings.data_dir),
    }


@app.post("/upload_files/{experiment_id}")
async def upload_files(
    experiment_id: int,
    files: List[UploadFile],
    app_settings: AppSettings = Depends(build_app_settings),
) -> Dict[str, Union[int, List[str]]]:
    await store_files(
        data_dir=app_settings.data_dir, experiment_id=experiment_id, files=files
    )
    return {
        "experiment_id": experiment_id,
        "filenames": [file.filename for file in files],
    }


@app.get("/get_files/{experiment_id}")
async def get_files(
    experiment_id: int,
    app_settings: AppSettings = Depends(build_app_settings),
) -> StreamingResponse:

    file_paths = get_experiment_files(app_settings.data_dir, experiment_id)
    zstream = await ZipFileStream.from_files(file_paths)

    return StreamingResponse(
        zstream,
        media_type="application/x-zip-compressed",
        headers={
            "Content-Disposition": f"attachment; filename=experiment{experiment_id}.zip"
        },
    )
