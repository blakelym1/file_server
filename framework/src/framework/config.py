from pydantic import BaseSettings
from anyio import Path
from functools import lru_cache
from dataclasses import dataclass
import tomli
from typing import Dict, Any


ROOT_DIR_ENV = "ROOT_DIR"
CONFIG_FILEPATH_ENV = "CONFIG_FILEPATH"


class EnvSettings(BaseSettings):
    root_dir: str = f"{Path(__file__).parents[2]}"
    config_filepath: str = f"{Path(__file__).parent / 'config.toml'}"

    @classmethod
    def load(cls) -> "EnvSettings":
        return cls()

    def load_config(self) -> Dict[str, Any]:
        with open(self.config_filepath, "rb") as f:
            app_settings_dict = tomli.load(f)
        return app_settings_dict


@dataclass(frozen=True)
class AppSettings:
    root_dir: Path
    data_dir: Path

    @classmethod
    def load(
        cls, root_dir_str: str, app_settings_dict: Dict[str, Any]
    ) -> "AppSettings":
        root_dir = Path(root_dir_str)
        data_dir = Path(app_settings_dict["data_dir"])
        if not data_dir.is_absolute():
            data_dir = root_dir.joinpath(data_dir)
        return cls(data_dir=data_dir, root_dir=root_dir)


@lru_cache
def build_app_settings() -> AppSettings:
    env_settings = EnvSettings.load()
    return AppSettings.load(
        root_dir_str=env_settings.root_dir, app_settings_dict=env_settings.load_config()
    )
