
[![pipeline status](https://gitlab.com/blakelym1/file_server/badges/main/pipeline.svg)](https://gitlab.com/blakelym1/file_server/-/commits/main) 
[![coverage report](https://gitlab.com/blakelym1/file_server/badges/main/coverage.svg)](https://gitlab.com/blakelym1/file_server/-/commits/main)

**Source Code**: <a href="https://gitlab.com/blakelym1/file_server/" target="_blank">https://gitlab.com/blakelym1/file_server/</a>

---

This is a project to test out ideas and for learning, it should not be used for real.